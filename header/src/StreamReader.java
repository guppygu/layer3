import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class StreamReader {

	public static IPHeader buildRecievedHeader(byte[] bytes) {
		IPHeader header = (bytes[0] >> 4) == 4 ? new IPv4Header() : new IPv6Header();
		// int[][] valMat = header.getValMat();

		long[] wordIntVals = new long[bytes.length / 4];
		long[][] valMat = header.getValMat();
		
		int mask = 0x0fffffff;

		for (int i = 0; i < wordIntVals.length ; i++) {
			byte[] temparray = new byte[8];
			for (int k = 0; k < 4; k++) {
				temparray[k + 4] = bytes[(i * 4) + k];
			}
			wordIntVals[i] = ByteBuffer.wrap(temparray).getLong() & 0xffffffff;
			for (int j = 0; j < header.valMat[i].length; j++) {

				long val = ((wordIntVals[i] & 0xffffffff) >> (32 - header.posMat[i][j])) & (j > 0 ? mask >>  (32 - (header.posMat[i][j] - header.posMat[i][j-1]) - 4) : wordIntVals[i] >> (32 - header.posMat[i][j])) ;
				valMat[i][j] =  val;
				
			}
		}
		
		if (valMat[0][0] == 4) {
			long sum =  header.calcCecksum(wordIntVals);
			valMat[2][2] = sum;
//			wordIntVals[2] |= sum & 0x0000FFFF;
			byte[] source = Arrays.copyOfRange(bytes, 12, 16);
			try {
				header.setSourceIP(InetAddress.getByAddress(source));
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			byte[] target = Arrays.copyOfRange(bytes, 16, 20);
			try {
				header.setTargetIP(InetAddress.getByAddress(target));
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else {
			byte[] source = Arrays.copyOfRange(bytes, 8, 24);
			try {
				header.setSourceIP(InetAddress.getByAddress(source));
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			byte[] target = Arrays.copyOfRange(bytes, 24, 40);
			try {
				header.setTargetIP(InetAddress.getByAddress(target));
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		header.setWordIntVals(wordIntVals);
		header.setValMat(valMat);
		return header;

	}

}
