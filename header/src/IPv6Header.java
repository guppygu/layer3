
public class IPv6Header extends IPHeader {
	
	

	public IPv6Header() {
					
		this.setPosMat(new long[][] {{4,12,32},{16,24,32},{32},{32},{32},{32},{32},{32},{32},{32}});		
		this.setFieldMat(new String[][] {{"Version", "Traffic Class", "Flow Label"}, 
			{"Payload Length", "Next Header", "Hop Limit"},
			{"Source IP 1"}, {"Source IP 2"}, {"Source IP 3"}, {"Source IP 4"},
			{"Target IP 1"}, {"Target IP 2"}, {"Target IP 3"}, {"Target IP 4"}});
		this.setValMat(MatrixOperation.zeroMatrix(this.posMat));
	}
	

}
