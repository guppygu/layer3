import java.nio.ByteBuffer;

public class MatrixOperation {
	
	public static long[][] zeroMatrix(long[][] m) {
		
		long[][] zeroMat = new long[m.length][];
		for (int i = 0; i < m.length; i++) {
			zeroMat[i] = m[i].clone();
			for(int j = 0; j < zeroMat[i].length; j++) {
				zeroMat[i][j] = 0;
			}
		}
		
		
		return zeroMat;
		
	
	}
	
	public static long[] returnLongfromByte(byte [] bytes, int start, int end) {

		long val[] = new long[bytes.length/4];
		for (int i = 0; i <bytes.length / 4 ; i++) {
			byte[] temparray = new byte[8];
			for (int k = 0; k < 4; k++) {
				temparray[k + 4] = bytes[(i * 4) + k];
			}
			val[i] = ByteBuffer.wrap(temparray).getLong() & 0xffffffff;
		}
		return val;
	}

}
