import java.nio.ByteBuffer;
import java.util.concurrent.ThreadLocalRandom;

public class ByteStream {
	byte[] bytes;

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public ByteStream(long[] wordIntVals) {

		ByteBuffer byteStream = ByteBuffer.allocate(wordIntVals.length * 4);
		for (long i : wordIntVals) {
			byteStream.put(ByteBuffer.allocate(4).putInt((int) i).array());

		}
		this.setBytes(byteStream.array());
	}

	public void printByteArray() {
		
		System.out.println("");
//		System.out.print("\t-----------------------------");
//		for (byte b : this.bytes) {
//			System.out.print("----");
//		}

		System.out.print("\n\t|=====>  Bytestream: ");
		
		for (byte b : this.bytes) {
			System.out.format("0x%x ", b);
		}
//		System.out.print(" |\n");
//		
//		System.out.print("\t-----------------------------");
//		for (byte b : this.bytes) {
//			System.out.print("----");
//		}
		System.out.println("");


	}
	
	public String corruptTheString() {
		
		this.bytes[ThreadLocalRandom.current().nextInt(0, bytes.length )] ^= 1 << ThreadLocalRandom.current().nextInt(0, 8);
		
		this.printByteArray();
		
		return "\n\tThe stream is corupted hahahahaha!\n";
		
	}

}
