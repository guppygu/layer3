

public class IPv4Header extends IPHeader {
	
	
	public IPv4Header() {					
		this.setPosMat(new long[][] {{4,8,16,32},{16,17,18,19,32},{8,16,32},{32},{32}});		
		this.setFieldMat(new String[][] {{"Version", "Header Length", "Service Type", "Total Length"}, 
			{"Identification", "Flag1", "Flag2", "Flag3", "Fragment Offset"},
			{"TTL", "Protocol", "Header Checksum"},
			{"Source IP"}, 
			{"Destination IP"}});
		

		this.setValMat(MatrixOperation.zeroMatrix(this.posMat));
	}
	

	

}
