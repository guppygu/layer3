import java.io.Console;
import java.net.InetAddress;
import java.net.UnknownHostException;


public class IPtools {

	public static InetAddress readIPAddress(Console cnsl) {

		String tempstring = cnsl.readLine("IP Address: ");
		InetAddress tempAdd = null;

		try {
			tempAdd = InetAddress.getByName(tempstring);

		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			System.out.println(
					"Invalid address! Please try again and conform to the stardard. If you have trouble typing, you can try yelling really loud, i guess.");
			tempAdd = readIPAddress(cnsl);

		}

		return tempAdd;

	}
	
}
