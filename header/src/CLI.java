
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.Console;

public class CLI {

	String[][] basicCommands = { { "credits", "prints out the credits" }, { "enable", "enters enable menu" },
			{ "quit", "exits the system" } };
	String[][] enableCommands = { { "disable", "exits the menu" }, { "hostname", "changes the hostname" },
			{ "configure terminal", "enter configuration mode" }, { "quit", "exits the system" }, 
			{ "testheader", "builds a testheader" } };
	String[][] confTCommands = { { "exit", "exits the menu" }, { "hostname", "changes the hostname" } };	
	String[] menuSigns = { ">", "#", " (config) #", ":" };
	String hostname = "coscoRouter";
	String enablePW = "1234QWer";
	Console cnsl;

	public CLI() {
		System.out.println("\n\nLoading Cosco Router...\n\n");
		cnsl = System.console();

		int i = 0;
		while (i == 0) {
			this.mainMenu();
		}

	}

	public void mainMenu() {
		System.out.println("");
		System.out.print(this.hostname + this.menuSigns[0] + " ");
		String tempstring = cnsl.readLine("");
		if (tempstring.matches("\\?.*")) {
			this.printMenu(this.basicCommands);
		} else if (tempstring.matches(".*quit.*")) {
			System.exit(0);
		} else if (tempstring.matches(".*credits.*")) {
			System.out.println("..............................................________........................ \r\n"
					+ "....................................,.-��...................``~.,.................. \r\n"
					+ ".............................,.-�...................................�-.,............ \r\n"
					+ ".........................,/...............................................�:,........ \r\n"
					+ ".....................,?......................................................\\,..... \r\n"
					+ ".................../...........................................................,}.... \r\n"
					+ "................./......................................................,:`^`..}.... \r\n"
					+ ".............../...................................................,:�........./..... \r\n"
					+ "..............?.....__.........................................:`.........../..... \r\n"
					+ "............./__.(.....�~-,_..............................,:`........../........ \r\n"
					+ ".........../(_....�~,_........�~,_....................,:`........_/........... \r\n"
					+ "..........{.._$;_......�=,_.......�-,_.......,.-~-,},.~�;/....}........... \r\n"
					+ "...........((.....*~_.......�=-._......�;,,./`..../�............../............ \r\n"
					+ "...,,,___.\\`~,......�~.,....................`.....}............../............. \r\n"
					+ "............(....`=-,,.......`........................(......;_,,-�............... \r\n"
					+ "............/.`~,......`-...............................\\....../\\................... \r\n"
					+ ".............\\`~.*-,.....................................|,./.....\\,__........... \r\n"
					+ ",,_..........}.>-._\\...................................|..............`=~-,.... \r\n"
					+ ".....`=~-,_\\_......`\\,.................................\\........................ \r\n"
					+ "...................`=~-,,.\\,...............................\\....................... \r\n"
					+ "................................`:,,...........................`\\..............__.. \r\n"
					+ ".....................................`=-,...................,%`>--==``....... \r\n"
					+ "........................................_\\..........._,-%.......`\\............... \r\n"
					+ "...................................,<`.._|_,-&``................`\\..............");
		} else if (tempstring.equals("")) {

		} else if (tempstring.equals("enable")) {
			this.enableMenuGate();

		} else if (this.basicCommands[1][0].matches(tempstring + ".*")) {
			this.enableMenuGate();

		} else {
			System.out.println("unrecognized command");

		}
		this.mainMenu();

	}

	public void enableMenuGate() {
		
		if (! this.enablePW.isEmpty()) {
	
//			String alpha = null;
			String pwd = this.enablePW;
			int wrongCounter = 0;
			
			while (wrongCounter >= 0) {
			
			if (this.cnsl != null) {
	            
	            // read line from the user input
	  //          alpha = cnsl.readLine("Name: ");
           
	            // read password into the char array
	           char [] pwd2 = cnsl.readPassword("\nPassword: ");
	            pwd = new String(pwd2);	  
	         } 
			if (! pwd.equals(this.enablePW)) {
				if (wrongCounter >= 2) {
				System.out.println("\nWrong Password!");	
				this.mainMenu();
				}
				else {
					wrongCounter++;
				
				}
			}
			else {
				enableMenuInside();
			}
			}
	
		}
		

	}
	
	public void enableMenuInside() {
		System.out.println("");
		System.out.print(this.hostname + this.menuSigns[1] + " ");
		String tempstring = this.cnsl.readLine("");
		if (tempstring.matches("\\?.*")) {
			this.printMenu(this.enableCommands);
		} else if (tempstring.matches(".*disable.*")) {
			this.mainMenu();
		} else if (tempstring.matches(".*hostname.*")) {
			hostname(tempstring);

		} else if (tempstring.matches(".*configure.*")) {
			confTMenu();

		} else if (tempstring.matches(".*quit.*")) {
			System.exit(0);
		} else if (tempstring.matches(".*testheader.*")) {
			HeaderTester ht = new HeaderTester();
			ht.testheader(this.cnsl);
		} else if (tempstring.equals("")) {

		} else {
			System.out.println("unrecognized command");

		}
		this.enableMenuInside();
		
	}

	public void confTMenu() {
		System.out.println("");
		System.out.print(this.hostname + this.menuSigns[2] + " ");
		String tempstring = this.cnsl.readLine("");
		if (tempstring.matches("\\?.*")) {
			this.printMenu(this.confTCommands);
		} else if (tempstring.matches(".*exit.*")) {
			this.enableMenuInside();
		} else if (tempstring.matches(".*hostname.*")) {
			hostname(tempstring);

		} else if (tempstring.equals("")) {

		} else {
			System.out.println("unrecognized command");

		}
		this.confTMenu();

	}

	public int returnCommandInd() {
		int index = 0;
		return index;

	}

	public void hostname(String teststring) {
		Pattern MY_PATTERN = Pattern.compile("hostname (.*)");
		Matcher m = MY_PATTERN.matcher(teststring);
		String s = this.hostname;
		while (m.find()) {
			s = m.group(1);
			// s now contains "BAR"

		}
		this.hostname = s;

	}
	
	public void printMenu(String[][] menu) {
		System.out.println("");
		for (String command[] : menu) {
			System.out.println(String.format("%-20s", command[0]).replace(" ", ".") + " " + command[1]);
		}
	}





	



}
