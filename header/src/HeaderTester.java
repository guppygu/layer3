import java.io.Console;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.Scanner;

public class HeaderTester {
	
	public  HeaderTester() {
		
		
	}
	
	public void testheader(Console cnsl) {
		IPHeader header = this.getIPAddresses(cnsl);


		// build an array of 32 bit words to store header
		header.BuildWords();
	
		// Print out binary and hex representations of header, as well as field information
		header.printFieldInfo();
		header.printWords();
		
		
	

		///		---Bitstream---
		//build a binary stream
		ByteStream binStream = new ByteStream(header.wordIntVals);
		
		//print stream		
		binStream.printByteArray();
		
		this.corruptStream(binStream);
		
		

		//		---Streamreader---
		
		IPHeader receivedHeader = StreamReader.buildRecievedHeader(binStream.bytes);
		
		receivedHeader.printWords();
		
		receivedHeader.printFieldInfo();
	
//		System.out.println("\n Header Checksum: " + InternetChecksum.calcCecksum(receivedHeader.wordIntVals));
		System.err.println(receivedHeader.valMat[0][0] == 4 ? (((header.valMat[2][2] ^ receivedHeader.valMat[2][2]) == 0 )? "\nChecksum OK" : "\nHeader Corrupt!" ): "");
		
	}
	
	public void corruptStream(ByteStream stream) {
		System.out.println("\n Do you want to corrupt the stream?  <yes/no>\n");
		System.out.print("coscoRouter> ");
		@SuppressWarnings("resource")
		Scanner reader = new Scanner(System.in);
		String tempstring = reader.next();
		System.out.println(tempstring.equals("yes") ? stream.corruptTheString() : " ");

	}
	
	public IPHeader getIPAddresses(Console cnsl) {

		// get the source and target ips
		InetAddress sourceIP;
		InetAddress targetIP;

		System.out.println("\nEnter the source IP Address. IPv4 or IPv6:\n");
		sourceIP = IPtools.readIPAddress(cnsl);
		System.out.println(sourceIP instanceof Inet4Address ? "\nEnter IPv4 target Address:\n"
				: "\nEnter IPv6 target Address: \n");
		targetIP = IPtools.readIPAddress(cnsl);

		// check if the ip versions match
		if (sourceIP instanceof Inet4Address != targetIP instanceof Inet4Address) {
			System.out.println(sourceIP instanceof Inet4Address ? "\nAddress type mismatch! Start over!\n"
					: "\nIPv6! That's the one with all the funny letters. Start over!\n");
			getIPAddresses(cnsl);
		}
		IPHeader header = sourceIP instanceof Inet4Address ? new IPv4Header() : new IPv6Header();
		header.setSourceIP(sourceIP);
		header.setTargetIP(targetIP);
		header.buildValMat();
		return header;

	}
	

}
