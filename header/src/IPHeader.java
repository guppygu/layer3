import java.net.Inet4Address;
import java.net.InetAddress;

public class IPHeader {
	
	//build words from shifting and combining values 
	
	static int wl = 32;
	long[] wordIntVals;
	long[][] valMat; 
	long[][] posMat;
	String[][] fieldMat;
	InetAddress sourceIP;
	InetAddress targetIP;

	
	
	
	public InetAddress getSourceIP() {
		return sourceIP;
	}

	public void setSourceIP(InetAddress sourceIP) {
		this.sourceIP = sourceIP;
	}

	public InetAddress getTargetIP() {
		return targetIP;
	}

	public void setTargetIP(InetAddress targetIP) {
		this.targetIP = targetIP;
	}

	public long[] getWordIntVals() {
		return wordIntVals;
	}

	public void setWordIntVals(long[] wordIntVals) {
		this.wordIntVals = wordIntVals;
	}
		
	public long[][] getValMat() {
		return valMat;
	}

	public void setValMat(long[][] valMat) {
		this.valMat = valMat;
	}
	
	public long[][] getPosMat() {
		return posMat;
	}

	public void setPosMat(long[][] posMat) {
		this.posMat = posMat;
	}
		
	
	public String[][] getFieldMat() {
		return fieldMat;
	}

	public void setFieldMat(String[][] fieldMat) {
		this.fieldMat = fieldMat;
	}

	public IPHeader() {
		
	}

	public void BuildWords() {
			long[] wordIntVals = new long[this.posMat.length];
		
			for (int i = 0; i < this.posMat.length; i++) {
				long temp = 0;
				for (int j = 0; j < this.posMat[i].length; j++) {
					long tempval = (long) this.valMat[i][j]; 
					temp = temp | ( (tempval & 0xffffffff) << (wl - posMat[i][j]) );				
				}	
				
				 wordIntVals[i] = temp;
			}
			
			if (this.valMat[0][0] == 4 ) {
				long sum = this.calcCecksum(wordIntVals);
				wordIntVals[2] |= sum & 0x0000FFFF;
				this.valMat[2][2] = sum;
			}
			this.wordIntVals = wordIntVals;
	}
	
	public void printChecksum() {
		System.out.println(this.valMat[0][0] == 4 ? ("\nIPv4 Header Checksum: " + this.valMat[2][2]) : "\nIPv6 Headers have no checksums");
		
	}
	
	public void printWords() {
		//print words out to screen
				System.out.println("\n\t-Header Binary Data:--------------------------------------------"
						+ "\n\t|\t\t\t\t\t\t\t\t|\n\t|\tb  0   4   8  12  16  20  24  28  31\tb  0  16  31\t|");
				//print binary 32 bit words
				for (int i = 0; i < this.wordIntVals.length; i++) {
				System.out.println("\t|\t" + (i + 1) + ": " + String.format("%32s", Long.toBinaryString(this.wordIntVals[i])).replace(" ", "0") + "\t" + (i + 1) + ": " + String.format("%08x", this.wordIntVals[i]) +  "\t|");	
				}
				System.out.println("\t|\t\t\t\t\t\t\t\t|\n\t ----------------------------------------------------------------");

			
	}
	
	public void printFieldInfo() {
		System.out.println("\n\tHeader Fields: \n");
		
		for (int i = 0; i <  this.fieldMat.length ; i++) {
			int j = 0;
			for (j = 0; j < this.fieldMat[i].length ; j ++) {
				if (! this.fieldMat[i][j].matches("(.*)IP(.*)")) {
				System.out.println("\t" + String.format("%-20s", this.fieldMat[i][j]).replace(" ", "-") + "> " + this.valMat[i][j]);
				}				
			}			
		}
		System.out.println("\t" + String.format("%-20s", "SourceIP").replace(" ", "-") + "> " + this.sourceIP.getHostAddress());
		System.out.println("\t" + String.format("%-20s", "TargetIP").replace(" ", "-") + "> " + this.targetIP.getHostAddress());
	}

	
	public void buildValMat() {
	

		long[] sourceLong = MatrixOperation.returnLongfromByte(this.sourceIP.getAddress(), 0, this.sourceIP.getAddress().length);
		long[] targetLong = MatrixOperation.returnLongfromByte(this.targetIP.getAddress(), 0, this.targetIP.getAddress().length);
					
		if (this.sourceIP instanceof Inet4Address) {
		//set header fields ipv4
		this.valMat[0][0] = 4; //version
		this.valMat[0][1] = 5; //IHL
		this.valMat[0][2] = 24;
		this.valMat[0][3] = 20; //Total Length
		this.valMat[1][0] = 344; //Identification
		this.valMat[1][2] = 1; //dont fragment flag
		this.valMat[2][0] = 32; //ttl
		this.valMat[2][1] = 6; //protocol 
		this.valMat[2][2] = 0; //checksum
		this.valMat[3][0] = sourceLong[0]; //source address
		this.valMat[4][0] = targetLong[0]; //target address
		
		}
		else {
			this.valMat[0][0] = 6;  //version
			this.valMat[0][1] = 24;	//traffic class
			this.valMat[0][2] = 1;	//flow label
			this.valMat[1][0] = 0;	//payload length
			this.valMat[1][1] = 0;	//next header
			this.valMat[1][2] = 32;  //hop limit
			this.valMat[2][0] = sourceLong[0]; //source address
			this.valMat[3][0] = sourceLong[1];
			this.valMat[4][0] = sourceLong[2];
			this.valMat[5][0] = sourceLong[3];
			this.valMat[6][0] = targetLong[0];	//target address
			this.valMat[7][0] =  targetLong[1];
			this.valMat[8][0] =  targetLong[2];
			this.valMat[9][0] =  targetLong[3];
			
		}
	
	
	}

	
	  public long calcCecksum(long[] wordIntVals) {	  
		  long sum = 0;
		  wordIntVals[2] &= 0xFFFF0000;
		  for (int i = 0; i < wordIntVals.length; i++) {
			  for (int j = 1; j >= 0; j--) {
				  sum += (wordIntVals[i] >> (j * 16)) & 0xFFFF;
				  sum = (sum & 0xFFFF) + (sum >> 17 & 0xF);	
			  }
			sum = ~sum & 0x0000FFFF;		  
		  }	  
		  return sum;
	  }

	

	
}
